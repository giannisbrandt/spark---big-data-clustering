import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.io.Serializable;
import java.util.*;

public class Main {

  public static void main(String[] args) {
      final SparkConf sparkConf = new SparkConf().setMaster("local").setAppName("myfirstsparkapp");
      JavaSparkContext sc = new JavaSparkContext(sparkConf);

      JavaRDD<String> lines = sc.textFile("/home/brandt/Desktop/data201718.csv");

      //get only those points that contain both x and y
      JavaRDD<String> filteredLines = lines.filter(new Function<String, Boolean>() {
          public Boolean call(String line) throws Exception {
              String[] split = line.split(",");
              if(split.length<2){
                  return false;
              }
              else if (split[0].equals("") || split[1].equals("\n")) {
                  return false;
              }
              return true;
          }
      });


      //get x values
      JavaRDD<Double> xValues = filteredLines.map(new Function<String, Double>() {
          public Double call(String line) throws Exception {
              String[] split = line.split(",");
              double xValue = Double.parseDouble(split[0]);
              return xValue;
          }
      });


      //get y values
      JavaRDD<Double> yValues = filteredLines.map(new Function<String, Double>() {
          public Double call(String line) throws Exception {
              String[] split = line.split(",");
              double yValue = Double.parseDouble(split[1]);
              return yValue;
          }
      });

      //get max and min for the data normalizing part
      Double maxX = xValues.max(new DummyComparator());
      Double minX = xValues.min(new DummyComparator());
      Double maxY = yValues.max(new DummyComparator());
      Double minY = yValues.min(new DummyComparator());

      //normalize data in the scale [0,1]
      List<Double> xCollection = xValues.collect();
      List<Double> yCollection = yValues.collect();
      List<Point> points = new ArrayList<Point>();
      for(int i=0;i<xCollection.size();i++){
          Point p = new Point(xCollection.get(i),yCollection.get(i));
          p=p.normalizePoint(minX,maxX,minY,maxY);
          points.add(p);
      }

      //pass the normalized points that ArrayList points contains into an RDD
      JavaRDD<Point> pointsRdd = sc.parallelize(points);

      //convert pointsRDD into JavaRDD<Vector> so we can pass these data in KMeansModel
      JavaRDD<Vector> data = pointsRdd.map(new Function<Point, Vector>(){
          public Vector call(Point s) throws Exception {
             Point p = new Point();
             p.setX(s.getX());
             p.setY(s.getY());
             return Vectors.dense(p.getX(),p.getY());
          }
      });

      data.cache();
      boolean k_setted = false;
      int numClusters = 2;
      int numIterations = 20;
      double previous_WSSE;

      //the following algorithm tries to find the "elbow point"
      //we stop creating clusters when the loss of WSSSE compared to the previous WSSSE is small
      KMeansModel clusters = KMeans.train(data.rdd(), numClusters, numIterations);
      double WSSSE = clusters.computeCost(data.rdd());
      previous_WSSE = WSSSE;
      double loss = 0;

      while (!k_setted){
          numClusters+=2;
          clusters = KMeans.train(data.rdd(), numClusters, numIterations);
          WSSSE = clusters.computeCost(data.rdd());
          if (numClusters == 4){
              loss = previous_WSSE - WSSSE;
          }

          if (previous_WSSE - WSSSE <(loss/6)){
              numClusters-=2;
              clusters = KMeans.train(data.rdd(), numClusters,numIterations);
              WSSSE = clusters.computeCost(data.rdd());
              k_setted = true;
          }
          else {
              previous_WSSE = WSSSE;
          }
      }

      //print centers of clusters as well as the clusters where they belong
      int cl =0;
      for(Vector center : clusters.clusterCenters()){
          System.out.println("Center for cluster " + cl + " is: " + center);
          cl++;
      }

      // saving the centers of the clusters in arraylist
      ArrayList<Point> centers = new ArrayList();

      for(int i=0;i<clusters.clusterCenters().length;i++){
          String s = clusters.clusterCenters()[i].toString();

          String[] split = s.split(",");
          split[0] = split[0].substring(1);
          split[1] = split[1].substring(0,split[1].length()-1);
          Point center = new Point();
          center.setX(Double.parseDouble(split[0]));
          center.setY(Double.parseDouble(split[1]));
          centers.add(center);

      }

      //creating an ArrayList to save all ArrayLists that contain points for each cluster respectively
      ArrayList<ArrayList<Point>> clusterLists = new ArrayList();
      for(int i=0;i<centers.size();i++){
          ArrayList<Point> list = new ArrayList();
          clusterLists.add(list);
      }

      //saving all points in an RDD
      List<Point> pointsList = pointsRdd.collect();
      HashMap<Point,Double> distances = new HashMap<>();

      //finding for each point the cluster that belongs to, so we can create the clusters with their points and
      // calculate the silhouette coefficient
      // also calculating the distance of each point from its cluster's center to find outliers

      ArrayList<ArrayList<Double>> distArr = new ArrayList<>();
      for(int i=0;i<centers.size();i++){
          ArrayList<Double> list = new ArrayList();
          distArr.add(list);
      }

      for(int i=0;i<pointsList.size();i++){
          double minDistance=Double.MAX_VALUE;
          int bestCenterIndex = -1;
          Point bestCenter = null;
          Point point = pointsList.get(i);

          Double distance = 0.0;
          for(int j=0;j<centers.size();j++){
              Double dis = point.distanceFrom(centers.get(j));
              distance = dis;
              if(dis<minDistance){
                  minDistance=dis;
                  bestCenter=centers.get(j);
                  bestCenter.normalizePoint(minX,maxX,minY,maxY);
                  bestCenterIndex=j;
              }

          }
         ArrayList<Point> pointsOfCluster = clusterLists.get(bestCenterIndex);
         pointsOfCluster.add(point);

         distArr.get(bestCenterIndex).add(distance);

         Point outl = new Point();
         outl.setX(point.getX()); outl.setY(point.getY()); outl.setCluster(bestCenterIndex);
         distances.put(outl,distance);
         clusterLists.set(bestCenterIndex,pointsOfCluster);
      }

      //calculating the parameters which are going to be used to find the outliers
      //the algorithm as well as the use of the parameters are explained in the pdf
      double[] medians = new double[distArr.size()];
      double[] lowerMedian = new double[distArr.size()];
      double[] upperMedian = new double[distArr.size()];
      double[] interRange = new double[distArr.size()];

      for(int i=0;i<distArr.size();i++){
          Collections.sort(distArr.get(i)); //sorting distances of every cluster from lowest to highest
          ArrayList<Double> getMedian = distArr.get(i);
          medians[i] = getMedian.get(getMedian.size()/2);
          lowerMedian[i] = getMedian.get(getMedian.size()/4);
          upperMedian[i] = getMedian.get((getMedian.size()/2 + getMedian.size()/4));
          interRange[i] = upperMedian[i] - lowerMedian[i];
          interRange[i] = interRange[i]*1.5;
          upperMedian[i] += interRange[i];
          lowerMedian[i] -= interRange[i];
      }



      double ai = 0;
      double bi = 0;
      double min = Double.MAX_VALUE;

      //Calculation of silhouette coefficient, a(i) and b(i) parameters
      for (int l=0;l<clusterLists.size();l++){
          ArrayList<Point> cluster = clusterLists.get(l);
          for (int i=0;i<cluster.size();i++){
              ai=0;
              bi=0;
              min = Double.MAX_VALUE;

              Point x = cluster.get(i);

              for (int j=0;j<cluster.size();j++){
                  if (x.getX() != cluster.get(j).getX() && x.getY() != cluster.get(j).getY()){
                      ai+=x.distanceFrom(cluster.get(j));
                  }
              }

              ai = ai/cluster.size(); //avg distance of i from all other points in the same cluster.

              //find distance of i from all points that dont belong in the same cluster with i.
              for (int a=0; a<clusterLists.size();a++){
                  if (a!=l){
                      ArrayList<Point> otherCluster = clusterLists.get(a);

                      for (int w=0;w<otherCluster.size();w++){
                          bi+=x.distanceFrom(otherCluster.get(w));
                      }

                      if (bi<min){
                          min = bi;
                      }
                  }
              }
          }
          //calculate the silhouette coefficient for this cluster depending on the type from wikipedia
          double totalSize = 0;
          for (int t=0;t<clusterLists.size();t++){
              if (t!=l){
                  totalSize+=clusterLists.get(t).size();
              }
          }
          min = min/totalSize;
          double bigger;
          if(ai>min){
              bigger=ai;
          }
          else{
              bigger=min;
          }
          double silhouette = (min-ai)/bigger;
          System.out.println("Average silhouette coefficient for cluster: " + l + " is " + silhouette);
      }

      //determining the outliers
      Iterator it = distances.entrySet().iterator();
      while (it.hasNext()) {
          Map.Entry pair = (Map.Entry)it.next();
          Point x = (Point) pair.getKey();
          double dist = (double) pair.getValue();
          int cluster = x.getCluster();
          if (dist > upperMedian[cluster]){
              System.out.println("Outlier for cluster " + cluster + " is the point: " + x.getX() + "," + x.getY());
          }
          it.remove();
      }
  }

}


class DummyComparator implements Serializable,Comparator<Double > {


    //This method returns the value 0 if o1 is numerically equal to o2;
    // a value less than 0 if o1 is numerically less than o2;
    // and a value greater than 0 if o1 is numerically greater than o2.
    @Override
    public int compare(Double o1, Double o2) {
        return Double.compare(o1, o2);
    }
}

class Point implements Serializable {
    private Double x;
    private Double y;
    private Double normalizedX;
    private Double normalizedY;
    private int cluster;

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public void setCluster(int cluster) { this.cluster = cluster; }

    public int getCluster() { return cluster; }

    public Double getNormalizedX(){return this.normalizedX;}
    public Double getNormalizedY(){return this.normalizedY;}
    public Point(){

    }
    public Point(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Double distanceFrom(Point p){
        Double diffX = this.getX()-p.getX();
        Double diffY = this.getY()-p.getY();
        double sqrt = Math.sqrt(diffX * diffX + diffY * diffY);
        return sqrt;
    }

    public Point normalizePoint(Double minX,Double maxX,Double minY,Double maxY){
        normalizedX = normalizeNumber(x,minX,maxX);
        normalizedY = normalizeNumber(y,minY,maxY);
        Point newPoint = new Point();
        newPoint.setX(normalizedX);
        newPoint.setY(normalizedY);
        return newPoint;
    }

    private Double normalizeNumber(Double x,Double min,Double max){
        return (x-min)/(max-min);
    }

    public String getNormalizedAsString(){
        return "("+normalizedX+","+normalizedY+")";
    }


    public void toString1(Point x){
        System.out.println(x.getX() + " " + x.getY());
    }

}